package android.net.wifi.p2p.wifisim.util;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.wifisim.SimWifiP2pDevice;
import android.net.wifi.p2p.wifisim.SimWifiP2pDeviceList;
import android.net.wifi.p2p.wifisim.SimWifiP2pManager;
import android.net.wifi.p2p.wifisim.sockets.SimWifiP2pSocket;
import android.net.wifi.p2p.wifisim.sockets.SimWifiP2pSocketManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by filipelteixeira on 29-09-2016.
 */
public class Conversions {

    public static ArrayList<WifiP2pDevice> simListToDeviceArrayList(SimWifiP2pDeviceList mList){
        ArrayList<SimWifiP2pDevice> deviceList = new ArrayList<SimWifiP2pDevice>(mList.getDeviceList());
        ArrayList<WifiP2pDevice> newList = new ArrayList<WifiP2pDevice>();

        for (SimWifiP2pDevice device: deviceList) {
            WifiP2pDevice newDevice = new WifiP2pDevice();

            newDevice.deviceName = device.deviceName;
            newDevice.deviceAddress = device.realDeviceAddress;
            newDevice.status = 0;

            newList.add(newDevice);
        }

        return newList;
    }

    public static WifiP2pDeviceList simListToDeviceList(SimWifiP2pDeviceList mList){
        ArrayList<SimWifiP2pDevice> deviceList = new ArrayList<SimWifiP2pDevice>(mList.getDeviceList());
        ArrayList<WifiP2pDevice> newList = new ArrayList<WifiP2pDevice>();

        for (SimWifiP2pDevice device: deviceList) {
            WifiP2pDevice newDevice = new WifiP2pDevice();

            newDevice.deviceName = device.deviceName;
            newDevice.deviceAddress = device.realDeviceAddress;
            newDevice.status = 0;

            newList.add(newDevice);
        }

        return new WifiP2pDeviceList(newList);
    }

    public static WifiP2pInfo getWifiInfo(SimWifiP2pSocketManager manager){
        WifiP2pInfo newInfo = new WifiP2pInfo();

        SimWifiP2pDeviceList socketList = manager.getConnectionsList();

        Log.d("WifiP2pManager", "Device list received from socket manager: " + socketList.toString());
        ArrayList<WifiP2pDevice> deviceList = Conversions.simListToDeviceArrayList(socketList);
        Log.d("WifiP2pManager", "Device list converted from socket manager: " + manager.getConnectionsList().toString());


        newInfo.groupFormed = true;
        newInfo.isGroupOwner = manager.getGroupInfo().askIsGO();

        if(!newInfo.isGroupOwner){
            Log.d("WifiP2pManager", " Group Owner is : + " + deviceList.get(1));
            try {
                newInfo.groupOwnerAddress =  InetAddress.getByName(deviceList.get(1).deviceAddress);
            } catch (UnknownHostException e) {
                Log.d("WifiP2pManager", " Group Owner address not found(other address) : + " + e.getMessage());
            }

        }
        else try {
            //if GO need to find own address
            Log.d(SimWifiP2pManager.TAG, " Looking for own InetAddress");
            newInfo.groupOwnerAddress = InetAddress.getByName(deviceList.get(0).deviceAddress);
            Log.d(SimWifiP2pManager.TAG, " Found own ip address: " + newInfo.groupOwnerAddress.getHostAddress());
        } catch (UnknownHostException e) {
            Log.d(SimWifiP2pManager.TAG, " Group Owner address not found(own address) : + " + e.getMessage());
        }

        return newInfo;
    }

    public static NetworkInfo getMockedNetworkInfo() {
        NetworkInfo newNetInfo = new NetworkInfo(ConnectivityManager.TYPE_WIFI, 1, "Mocked WifiDirect", "Mocked SubType");
        newNetInfo.setState(NetworkInfo.State.CONNECTED);

        return newNetInfo;
    }

    public static WifiP2pGroup getWifiP2pGroup(SimWifiP2pSocketManager manager){
        WifiP2pGroup newGroup = new WifiP2pGroup();

        ArrayList<WifiP2pDevice> deviceList = Conversions.simListToDeviceArrayList(manager.getConnectionsList());
        Log.d("WifiP2pManager", "Device list received from socket manager: " + manager.toString());
        for (WifiP2pDevice d : deviceList) newGroup.addClient(d);
        newGroup.setNetworkName("termite");

        if(!manager.getGroupInfo().askIsGO()){
            // if not GO need to find GO
            for (WifiP2pDevice d : deviceList)
                if(d.isGroupOwner()){
                    newGroup.setOwner(d);
                }
        }

        return newGroup;
    }
}
