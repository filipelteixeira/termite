package android.net.wifi.p2p.wifisim.service;

/**
 * Created by filipelteixeira on 06-06-2016.
 */

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.wifisim.SimWifiP2pBroadcastReceiver;
import android.net.wifi.p2p.wifisim.SimWifiP2pManager;
import android.net.wifi.p2p.wifisim.util.EmulationController;
import android.os.IBinder;
import android.util.Log;

public class SimWifiP2pEventService extends Service {
    boolean mAllowRebind;
    IBinder mBinder;
    private SimWifiP2pBroadcastReceiver mReceiverTermite;
    int mStartMode;

    public SimWifiP2pEventService() {
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Termite Event Service created");
    }

    public IBinder onBind(Intent var1) {
        return this.mBinder;
    }

    public void onCreate() {
        EmulationController.setUsed(true);
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Termite Event Service initiated");
        IntentFilter intent = new IntentFilter();
        intent.addAction("android.net.wifi.p2p.wifisim.STATE_CHANGED");
        intent.addAction("android.net.wifi.p2p.wifisim.PEERS_CHANGED");
        intent.addAction("android.net.wifi.p2p.wifisim.NETWORK_MEMBERSHIP_CHANGED");
        intent.addAction("android.net.wifi.p2p.wifisim.GROUP_OWNERSHIP_CHANGED");
        this.mReceiverTermite = new SimWifiP2pBroadcastReceiver();
        this.registerReceiver(this.mReceiverTermite, intent);
        Log.d(SimWifiP2pManager.TAG, "Event Service: Termite Receiver registered");
    }

    public void onDestroy() {
        this.unregisterReceiver(this.mReceiverTermite);
        EmulationController.setEventServiceRunning(false);
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Termite Event Receiver unregistered");
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Destroyed");
    }

    public void onRebind(Intent var1) {
    }

    public int onStartCommand(Intent intent, int var2, int var3) {
        EmulationController.setEventServiceRunning(true);
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Start command called");
        return this.mStartMode;
    }

    public boolean onUnbind(Intent intent) {
        return this.mAllowRebind;
    }
}

