package android.net.wifi.p2p.wifisim;

/**
 * Created by filipelteixeira on 06-06-2016.
*/

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.wifisim.sockets.SimWifiP2pSocketManager;
import android.net.wifi.p2p.wifisim.util.Conversions;
import android.util.Log;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Set;

import static android.widget.Toast.makeText;

@TargetApi(14)
public class SimWifiP2pBroadcastReceiver extends BroadcastReceiver {
    SimWifiP2pSocketManager socketManager = new SimWifiP2pSocketManager();

    public SimWifiP2pBroadcastReceiver() {
        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Termite Broadcast Receiver created");
    }

    public void onReceive(Context context, Intent intent) {
        String state = intent.getAction();
        if(SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION.equals(state)) {
            Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Received SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION, " +
                    "transmitting WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION");

            boolean wifion = false;
            int wifiState = intent.getIntExtra(SimWifiP2pBroadcast.EXTRA_WIFI_STATE, -1);
            if (wifiState == SimWifiP2pBroadcast.WIFI_P2P_STATE_ENABLED) {
                wifion = true;
            } else {
                wifion = false;
            }
            socketManager.handleActionStateChanged(wifion);

            Intent broadcast = new Intent(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            int termiteWifiState = intent.getIntExtra(SimWifiP2pBroadcast.EXTRA_WIFI_STATE, -1);

            if(termiteWifiState != 1 && termiteWifiState != 2)
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED " +
                        "caught with unsupported state");

            broadcast.putExtra(WifiP2pManager.EXTRA_WIFI_STATE, termiteWifiState);
            context.sendBroadcast(broadcast);

            makeText(context, "Termite: WiFi State Changed", Toast.LENGTH_SHORT).show();

        } else {
            if("android.net.wifi.p2p.wifisim.PEERS_CHANGED".equals(state)) {
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Received SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION, " +
                        "transmitting WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION");

                SimWifiP2pDeviceList dList = (SimWifiP2pDeviceList) intent.getSerializableExtra(
                        SimWifiP2pBroadcast.EXTRA_DEVICE_LIST);
                socketManager.handleActionPeersChanged(dList);
                context.sendBroadcast(new Intent(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION));

                makeText(context, "Termite: Peer list changed", Toast.LENGTH_SHORT).show();
                return;
            }

            if("android.net.wifi.p2p.wifisim.NETWORK_MEMBERSHIP_CHANGED".equals(state)) {

                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Debug point 1");
                SimWifiP2pInfo info = (SimWifiP2pInfo) intent.getSerializableExtra(SimWifiP2pBroadcast.EXTRA_GROUP_INFO);
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Debug point, got info isGO: " + info.askIsGO());

                if(info.askIsGO()) {
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Ignoring network membership change for Group Owner");
                    makeText(context, "Termite: Network membership changed", Toast.LENGTH_SHORT).show();
                }
                else{
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Received SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION, " +
                            "transmitting WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION and recreating action");

                    Intent wifiConnectionStateChanged = new Intent(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

                    socketManager.handleActionGroupMembershipChanged(info);

                    NetworkInfo newNetInfo = new NetworkInfo(ConnectivityManager.TYPE_WIFI, 1, "Mocked WifiDirect", "Mocked SubType");
                    newNetInfo.setState(NetworkInfo.State.CONNECTED);

                    WifiP2pInfo newInfo = new WifiP2pInfo();
                    WifiP2pGroup newGroup = new WifiP2pGroup();

                    ArrayList<WifiP2pDevice> deviceList = Conversions.simListToDeviceArrayList(socketManager.getConnectionsList());
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + "Device list received from socket manager: " +
                            socketManager.getConnectionsList().toString());
                    for (WifiP2pDevice d : deviceList) newGroup.addClient(d);
                    newGroup.setNetworkName("termite");

                    newInfo.groupFormed = true;
                    newInfo.isGroupOwner = info.askIsGO();
                    if(!newInfo.isGroupOwner){
                        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Group Owner is : + " + deviceList.get(1));
                        newGroup.setOwner(deviceList.get(1));
                        try {
                            newInfo.groupOwnerAddress =  InetAddress.getByName(deviceList.get(1).deviceAddress);
                        } catch (UnknownHostException e) {
                            Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Group Owner address not found(other address) : + " + e.getMessage());
                        }

                    }
                    else try {
                        //if GO need to find own address
                        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Looking for own InetAddress");
                        newInfo.groupOwnerAddress = InetAddress.getByName(deviceList.get(0).deviceAddress);
                        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + "Found own ip address: " + newInfo.groupOwnerAddress.getHostAddress());
                    } catch (UnknownHostException e) {
                        Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Group Owner address not found(own address) : + " + e.getMessage());
                    }

                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": information in SimWifiP2pInfo:\n" + info.printable());

                    newGroup.setIsGroupOwner(info.askIsGO());
                    Set mGroupClients = info.getDevicesInNetwork();
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + mGroupClients.toString());


                    wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_NETWORK_INFO,
                            newNetInfo);
                    wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO, newInfo);
                    wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP, newGroup);

                    context.sendBroadcast(wifiConnectionStateChanged);
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Sent connection event");
                    makeText(context, "Termite: Group membership changed", Toast.LENGTH_SHORT).show();
                }

                return;
            }

            if("android.net.wifi.p2p.wifisim.GROUP_OWNERSHIP_CHANGED".equals(state)) {
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Received SimWifiP2pBroadcast.WIFI_P2P_GROUP_OWNERSHIP_CHANGED_ACTION, " +
                    "transmitting WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION");

                Intent wifiConnectionStateChanged = new Intent(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
                SimWifiP2pInfo info = (SimWifiP2pInfo) intent.getSerializableExtra(SimWifiP2pBroadcast.EXTRA_GROUP_INFO);

                socketManager.handleActionGroupOwnershipChanged(info);

                NetworkInfo newNetInfo = new NetworkInfo(ConnectivityManager.TYPE_WIFI, 1, "Mocked WifiDirect", "Mocked SubType");
                newNetInfo.setState(NetworkInfo.State.CONNECTED);

                WifiP2pInfo newInfo = new WifiP2pInfo();
                WifiP2pGroup newGroup = new WifiP2pGroup();

                ArrayList<WifiP2pDevice> deviceList = Conversions.simListToDeviceArrayList(socketManager.getConnectionsList());
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + "Device list received from socket manager: " +
                        socketManager.getConnectionsList().toString());
                for (WifiP2pDevice d : deviceList) newGroup.addClient(d);
                newGroup.setNetworkName("termite");

                newInfo.groupFormed = true;
                newInfo.isGroupOwner = info.askIsGO();
                if(!newInfo.isGroupOwner){
                    // if not GO need to find GO
                    for (WifiP2pDevice d : deviceList)
                        if(d.isGroupOwner()){
                            newGroup.setOwner(d);
                            try {
                                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Found GO device. Looking for own InetAddress");
                                newInfo.groupOwnerAddress = InetAddress.getByName(d.deviceAddress);
                            } catch (UnknownHostException e) {
                                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Group Owner address not found(other address): + " + e.getMessage());
                            }
                        }
                }
                else try {
                    //if GO need to find own address
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Looking for own InetAddress");
                    newInfo.groupOwnerAddress = InetAddress.getLocalHost();
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + "Found own ip address: " + newInfo.groupOwnerAddress.getHostAddress());
                } catch (UnknownHostException e) {
                    Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": Group Owner address not found(own address) : + " + e.getMessage());
                }

                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + ": information in SimWifiP2pInfo:\n" + info.printable());

                newGroup.setIsGroupOwner(info.askIsGO());
                Set mGroupClients = info.getDevicesInNetwork();
                Log.d(SimWifiP2pManager.TAG, this.getClass().getSimpleName() + mGroupClients.toString());


                wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_NETWORK_INFO,
                        newNetInfo);
                wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO, newInfo);
                wifiConnectionStateChanged.putExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP, newGroup);

                context.sendBroadcast(wifiConnectionStateChanged);
                makeText(context, "Termite: Group ownership changed", Toast.LENGTH_SHORT).show();
            }
        }

    }
}

