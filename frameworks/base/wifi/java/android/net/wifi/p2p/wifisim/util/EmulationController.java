package android.net.wifi.p2p.wifisim.util;

public class EmulationController{
    //region Declarations
    private static boolean emulated = false;
    private static boolean altered = false;
    private static boolean used = false;
    private static boolean eventServiceRunning = false;
    private static boolean serviceRunning = false;
    //todo: need lock?
    //endregion

    //region Constructor
    public EmulationController() {
    }
    //endregion

    //region Public Access Methods
    public static boolean isEmulated(){
        return emulated;
    }

    public static boolean isAltered() {
        return altered;
    }

    public static boolean isUsed() {
        return used;
    }

    public static void setEmulated(boolean emulated) {
        EmulationController.emulated = emulated;
    }

    public static void setAltered(boolean altered) { EmulationController.altered = altered;
    }

    public static void setUsed(boolean used) {
        if(used) setAltered(true);
        EmulationController.used = used;
    }

    public static boolean isServiceRunning() {
        return serviceRunning;
    }

    public static void setServiceRunning(boolean serviceRunning) {
        EmulationController.serviceRunning = serviceRunning;
    }

    public static boolean isEventServiceRunning() {
        return eventServiceRunning;
    }

    public static void setEventServiceRunning(boolean eventServiceRunning) {
        EmulationController.eventServiceRunning = eventServiceRunning;
    }
    //endregion
}