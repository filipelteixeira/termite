/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.termite;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.wifisim.SimWifiP2pManager;
import android.net.wifi.p2p.wifisim.service.SimWifiP2pEventService;
import android.net.wifi.p2p.wifisim.service.SimWifiP2pService;
import android.net.wifi.p2p.wifisim.util.EmulationController;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.util.Log;
import android.widget.Toast;

import static android.widget.Toast.makeText;

/**
 * TermiteEnabler is a helper to manage the Termite emulation on/off checkbox preference. It is
 * turns on/off Termite and ensures the summary of the preference reflects the
 * current state.
 */
public class TermiteEnabler implements Preference.OnPreferenceChangeListener {
    private final Context mContext;
    private final SwitchPreference mSwitch;
    private final IntentFilter mIntentFilter;

    private final Intent termiteEventService;
    private final Intent termiteService;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    public TermiteEnabler(Context context, SwitchPreference switchPreference) {
        mContext = context;
        mSwitch = switchPreference;
        mIntentFilter = new IntentFilter();
        mSwitch.setOnPreferenceChangeListener(this);
        mSwitch.setChecked(EmulationController.isEmulated());
        termiteEventService = new Intent(mContext, SimWifiP2pEventService.class);
        termiteService = new Intent(mContext, SimWifiP2pService.class);
        Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": Termite enabler instanced");

        if(EmulationController.isUsed()) mSwitch.setSwitchTextOff("Need restart to be modified");
    }

    public void resume() {
        Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": Termite enabler resumed");

        mContext.registerReceiver(mReceiver, mIntentFilter);
        mSwitch.setOnPreferenceChangeListener(this);
    }

    public void pause() {
        Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": Termite enabler paused");

        mContext.unregisterReceiver(mReceiver);
        mSwitch.setOnPreferenceChangeListener(null);
    }

    public boolean onPreferenceChange(Preference preference, Object value) {
        // Turn Termite on/off

        makeText(mContext, "Changing Termite properties", Toast.LENGTH_SHORT).show();

        final boolean desiredState = (Boolean) value;
        Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": pref changed with value " + desiredState);

        mSwitch.setEnabled(false);
        if (desiredState) {
            if(EmulationController.isUsed()){
                makeText(mContext, "Wifi Direct/Termite was already used, must restart to change", Toast.LENGTH_SHORT).show();
                mSwitch.setEnabled(false);
            }
            else{
                EmulationController.setEmulated(true);
                if(!EmulationController.isEventServiceRunning())
                    mContext.startService(termiteEventService);
                if(!EmulationController.isServiceRunning())
                    mContext.startService(termiteService);
                makeText(mContext, "Wifi Direct/Termite ON", Toast.LENGTH_SHORT).show();
            }
        } else if (EmulationController.isUsed()) {
            makeText(mContext, "Wifi Direct/Termite was already used, must restart to change", Toast.LENGTH_SHORT).show();
            mSwitch.setEnabled(false);
        } else {
            EmulationController.setEmulated(false);
            if(!mContext.stopService(termiteEventService))
                Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": Termite Event Service stop has failed");
            if(!mContext.stopService(termiteService))
                Log.w(SimWifiP2pManager.TAG, getClass().getSimpleName() + ": Termite Service stop has failed");

            makeText(mContext, "Wifi Direct/Termite OFF", Toast.LENGTH_SHORT).show();
        }

        mSwitch.setChecked(EmulationController.isEmulated());
        mSwitch.setEnabled(!EmulationController.isUsed());

        if(EmulationController.isUsed()) mSwitch.setSwitchTextOff("Need restart to be modified");

        return false;
    }

}
